import React, {useState, Component} from 'react';
import Total from './total';
import Keypad from './keypad';
import Receipt from './receipt';
import './global';

//class App extends Component{
//  render() {
function App() {
    const [total, setTotal] = useState(0);
    const [keypadTotal, setKeypadTotal] = useState(0);

    const [items, setItems] = useState([
      /*{value: 1.23},
      {value: 4.56},
      {value: 2.15},
      {value: 7.89}*/
    ]);

    const update = () => {
      setKeypadTotal(Number(global.keypadTotal));
      setItems(items => [...items, {value: global.keypadTotal}]);

      console.log(items);

      setTotal(items.map(li => li.value).reduce((sum, val) => sum + val, 0));

      //global.keypadTotal = 0;
      //setKeypadTotal(0);
    };

    return(
      <div className="app">
        <div className="register-container border-radius">
          <div className="total-container">
            <Total />
          </div>
          <div className="keypad">
            <Keypad />
          </div>
        </div>
        <div className="receipt-container">
          {items.map(item => (
            <Receipt value={item.value} />
          ))}
          <div class="receipt-total">{total}</div>
        </div>
        <div class="comments">
            <p>Please open the console, you'll see some activity going on there.</p>
            <p>You can click the numbers, and then hit update.</p>
            <p>Not quite there, havent quite pulled all things together and ironed out quirks, but thats 3 hours :)</p>
            <button onClick={update}>update</button>
          </div>
      </div>
    );
//  }
}

export default App;
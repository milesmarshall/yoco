import React from 'react';
import './App.css';

function Keypad(){

  function add(e){
    if (global.keypadTotal === 0)
      global.keypadTotal = e;
    else
      global.keypadTotal += e;

    console.log(global.keypadTotal);
  };

  return(
    <container>
      <block>
      <button onClick={add.bind(this, '1')}>1</button>
      </block>
      <block>
      <button onClick={add.bind(this, '2')}>2</button>
      </block>
      <block last="">
      <button onClick={add.bind(this, '3')}>3</button>
      </block>
      <block>
      <button onClick={add.bind(this, '4')}>4</button>
      </block>
      <block>
      <button onClick={add.bind(this, '5')}>5</button>
      </block>
      <block last="">
      <button onClick={add.bind(this, '6')}>6</button>
      </block>
      <block>
      <button onClick={add.bind(this, '7')}>7</button>
      </block>
      <block>
      <button onClick={add.bind(this, '8')}>8</button>
      </block>
      <block last="">
      <button onClick={add.bind(this, '9')}>9</button>
      </block>
      <block bottom="">
      <button onClick={add.bind(this, '-')}>-</button>
      </block>
      <block bottom="">
      <button onClick={add.bind(this, '0')}>0</button>
      </block>
      <block bottom="" last="">
        <button onClick={add.bind(this, '+')}>+</button>
      </block>
  </container>
  )
}

export default Keypad;

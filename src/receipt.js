import React from 'react';
import './App.css';

function Receipt({value}){
  return(
    <div class="receipt-items">
      <p>{value}</p>
    </div>
  );
}

export default Receipt;